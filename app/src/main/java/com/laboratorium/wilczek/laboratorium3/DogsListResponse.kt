package com.laboratorium.wilczek.laboratorium3

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class DogsListResponse {
    @SerializedName("status")
    @Expose
    private var status: String? = null
    @SerializedName("message")
    @Expose
    private var message: List<String>? = null

    fun getStatus(): String? {
        return status
    }

    fun setStatus(status: String) {
        this.status = status
    }

    fun getMessage(): List<String>? {
        return message
    }

    fun setMessage(message: List<String>) {
        this.message = message
    }
}